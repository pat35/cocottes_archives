## Cocote version Pagnol ##

### Source d'inspiration ###
. [pour l'idée](http://archives.orleans-metropole.fr/r/640/cocottes-/) 

. [pour la création de cette cocotte](https://www.mademoiselle-dentelle.fr/realise-un-menu-cocotte-en-papier-sous-power-point-le-tutoriel-de-madame-pivoine/)


### Documents à télécharger ###
1. [Version pptx](https://framagit.org/Mac_Graveur/cocottes_archives/-/blob/master/Cocotte_pagnol/cocotte_pagnol.pptx)
2. [Version pdf](https://framagit.org/Mac_Graveur/cocottes_archives/-/blob/master/Cocotte_pagnol/cocotte_pagnol.pdf)

### Questions ###
1. En quelle année est né M.Pagnol ? [1895](https://fr.wikipedia.org/wiki/Marcel_Pagnol)
2. Qui est le fils de Marius ? [César](https://fr.wikipedia.org/wiki/C%C3%A9sar_(film,_1936))
3. Quelle actrice est Manon en 1986 ? [Emmanuelle Béart](https://fr.wikipedia.org/wiki/Manon_des_sources_(film,_1986))
4. Qui est le photographe de M.Pagnol ? [Henri Moiroud](https://data.bnf.fr/fr/13543671/henri_moiroud/)
5. Où est né M.Pagnol ? [Aubagne](https://fr.wikipedia.org/wiki/Marcel_Pagnol)
6.  Quel est le nom du  château de ma mère  ? [Château de la Buzine](https://fr.wikipedia.org/wiki/Ch%C3%A2teau_de_la_Buzine)
7. Tu tires ou tu pointes ? [A chaque son style](https://www.dailymotion.com/video/x12slwt)
8. Quel est le lycée de M.Pagnol ? [Lycée Thiers](https://fr.wikipedia.org/wiki/Lyc%C3%A9e_Thiers)


### Illustration ###
 [AD 13 7 Fi 1277](http://www.archives13.fr/ark:/40700/vta9bf7a4ad147424bf/dao/0/1) H.Moiroud : Plan moyen, scène de tournage, Jacqueline Pagnol et Raymond Pellegrin attablés.